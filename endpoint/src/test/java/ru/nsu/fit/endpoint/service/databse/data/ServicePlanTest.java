package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.apache.commons.lang.RandomStringUtils;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlanException;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class ServicePlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewServicePlan() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 100, 10, 50);
    }

    @Test
    public void testCreateNewServicePlanWithWrongSymbolsInName() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.SYMBOLS_NAME_MESSAGE);

        new ServicePlan("Plan n@me", "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithLongBadName() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.LONG_NAME_MESSAGE);

        new ServicePlan(RandomStringUtils.randomAlphabetic(129), "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithLongGoodName() throws ServicePlanException {
        new ServicePlan(RandomStringUtils.randomAlphabetic(128), "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithShortBadName() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.SHORT_NAME_MESSAGE);

        new ServicePlan("P", "Details plan", 1, 1, 1);
    }
    @Test
    public void testCreateNewServicePlanWithShortGoodName() throws ServicePlanException {
        new ServicePlan("Pa", "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithLongBadDetails() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.LONG_DETAILS_MESSAGE);

        new ServicePlan("Plan name", RandomStringUtils.random(1025), 1, 1, 1);
    }
    @Test
    public void testCreateNewServicePlanWithLongGoodDetails() throws ServicePlanException {
        new ServicePlan("Plan name", RandomStringUtils.random(1024), 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithShortBadDetails() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.SHORT_DETAILS_MESSAGE);

        new ServicePlan("Plan name", "", 1, 1, 1);
    }
    @Test
    public void testCreateNewServicePlanWithShortGoodDetails() throws ServicePlanException {
        new ServicePlan("Plan name", "D", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithBigBadMaxSeats() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.BIG_MAXSEATS_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1000000, 1, 1);
    }
    @Test
    public void testCreateNewServicePlanWithBigGoodMaxSeats() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 999999, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithSmallBadMaxSeats() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.SMALL_MAXSEATS_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 0, 1, 1);
    }
    @Test
    public void testCreateNewServicePlanWithSmallGoodMaxSeats() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithBigBadMinSeats() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.BIG_MINSEATS_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1, 10000000, 1);
    }

    @Test
    public void testCreateNewServicePlanWithBigGoodMinSeats() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 999999, 999999, 1);
    }

    @Test
    public void testCreateNewServicePlanWithSmallBadMinSeats() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.SMALL_MINSEATS_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1, 0, 1);
    }
    @Test
    public void testCreateNewServicePlanWithSmallGoodMinSeats() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 1, 1, 1);
    }

    @Test
    public void testCreateNewServicePlanWithMinSeatsGreaterThanMaxSeats() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.MINSEATS_MORE_MAXSEATS_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1, 2, 1);
    }

    @Test
    public void testCreateNewServicePlanWithNegativeFee() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.NEGATIVE_FEE_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1, 1, -1);
    }
    @Test
    public void testCreateNewServicePlanWithSmallGoodFee() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 1, 1, 0);
    }

    @Test
    public void testCreateNewServicePlanWithBigBadFee() throws ServicePlanException {
        expectedEx.expect(ServicePlanException.class);
        expectedEx.expectMessage(ServicePlanException.BIG_FEE_MESSAGE);

        new ServicePlan("Plan name", "Details plan", 1, 1, 1000000);
    }
    @Test
    public void testCreateNewServicePlanWithBigGoodFee() throws ServicePlanException {
        new ServicePlan("Plan name", "Details plan", 1, 1, 999999);
    }
}
