package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.exceptions.CustomerException;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws CustomerException {
        new Customer("John", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.SHORT_PASS_MESSAGE);
        new Customer("John", "Wick", "john_wick@gmail.com", "Aa123", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortGoodPass() throws CustomerException {
        new Customer("John", "Wick", "john_wick@gmail.com", "Aa1231", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.LONG_PASS_MESSAGE);
        new Customer("John", "Wick", "john_wick@gmail.com", "123QWE123qwe1", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongGoodPass() throws CustomerException {
        new Customer("John", "Wick", "john_wick@gmail.com", "123QWE123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithPassContainsLogin() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.LOGIN_PASS_MESSAGE);
        new Customer("John", "Wick", "a@ya.com", "A1a@ya.com", 0);
    }

    @Test
    public void testCreateNewCustomerWithPassContainsFirstName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.FIRSTNAME_PASS_MESSAGE);
        new Customer("John", "Wick", "a@ya.com", "John123", 0);
    }

    @Test
    public void testCreateNewCustomerWithPassContainsLastName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.LASTNAME_PASS_MESSAGE);
        new Customer("John", "Wick", "a@ya.com", "Wick123", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.EASY_PASS_MESSAGE);
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithNegativeBalance() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.NEGATIVE_BALANCE_MESSAGE);
        new Customer("John", "Wick", "john_wick@gmail.com", "QWE123qwe", -1);
    }
    @Test
    public void testCreateNewCustomerWithNegativeBalanceGood() throws CustomerException {
        new Customer("John", "Wick", "john_wick@gmail.com", "QWE123qwe", 1);
    }

    @Test
    public void testCreateNewCustomerWithInvalidLogin() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.INVALID_LOGIN_MESSAGE);
        new Customer("John", "Wick", "john_wick", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithInvalidLoginSecond() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.INVALID_LOGIN_MESSAGE);
        new Customer("John", "Wick", "john_wick@@gmail.com", "QWE123qwe", 0);
    }

    // firstname
    @Test
    public void testCreateNewCustomerWithSymbolsFirstName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.SYMBOLS_FIRSTNAME_MESSAGE);
        new Customer("Jo1hn", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortFirstName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.SHORT_FIRSTNAME_MESSAGE);
        new Customer("J", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortGoodFirstName() throws CustomerException {
        new Customer("Jo", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongFirstName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.LONG_FIRSTNAME_MESSAGE);
        new Customer("Johnanindjeva", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongGoodFirstName() throws CustomerException {
        new Customer("Johnanindjev", "Wick", "john_wick@gmail.com", "QWE123qwe", 0);
    }

    // lastname
    @Test
    public void testCreateNewCustomerWithSymbolsLastName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.SYMBOLS_LASTNAME_MESSAGE);
        new Customer("John", "Wi1ck", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortLastName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.SHORT_LASTNAME_MESSAGE);
        new Customer("John", "W", "john_wick@gmail.com", "QE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithShortGoodLastName() throws CustomerException {
        new Customer("John", "Wi", "john_wick@gmail.com", "QE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongLastName() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage(CustomerException.LONG_LASTNAME_MESSAGE);
        new Customer("John", "Wickanindjeva", "john_wick@gmail.com", "QWE123qwe", 0);
    }
    @Test
    public void testCreateNewCustomerWithLongGoodLastName() throws CustomerException {
        new Customer("John", "Wickanindjev", "john_wick@gmail.com", "QWE123qwe", 0);
    }
}
