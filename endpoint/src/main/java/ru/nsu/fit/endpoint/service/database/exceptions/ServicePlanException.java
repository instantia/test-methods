package ru.nsu.fit.endpoint.service.database.exceptions;

public class ServicePlanException extends ObjectException {
    public static final String LONG_NAME_MESSAGE = "Name shouldn't be longer than 128 symbols";
    public static final String SHORT_NAME_MESSAGE = "Name should be at least 2 symbols";
    public static final String SYMBOLS_NAME_MESSAGE = "Name shouldn't contain special symbols";
    public static final String LONG_DETAILS_MESSAGE = "Details shouldn't be longer than 1024 symbols";
    public static final String SHORT_DETAILS_MESSAGE = "Name should be at least 1 symbol";
    public static final String BIG_MAXSEATS_MESSAGE = "Max seats shouldn't be bigger than 9999999";
    public static final String SMALL_MAXSEATS_MESSAGE = "Max seats shouldn't be less than 1";
    public static final String BIG_MINSEATS_MESSAGE = "Min seats shouldn't be bigger than 9999999";
    public static final String SMALL_MINSEATS_MESSAGE = "Min seats shouldn't be less than 1";
    public static final String MINSEATS_MORE_MAXSEATS_MESSAGE = "Min seats shouldn't be greater than max seats";
    public static final String NEGATIVE_FEE_MESSAGE = "Fee can't be less than 0";
    public static final String BIG_FEE_MESSAGE = "Fee can't be greater than 999999";

    public ServicePlanException(String msg) {
        super(msg);
    }
}