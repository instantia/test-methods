package ru.nsu.fit.endpoint.service.database.exceptions;

public class CustomerException extends ObjectException {
    public static final String EMPTY_PASS_MESSAGE = "Password mustn't be empty";
    public static final String SHORT_PASS_MESSAGE = "Password's length should be more or equal 6 symbols";
    public static final String LONG_PASS_MESSAGE = "Password's length should be more or equal 6 symbols and less or equal 12 symbols";
    public static final String LOGIN_PASS_MESSAGE = "Password contains login";
    public static final String FIRSTNAME_PASS_MESSAGE = "Password contains firstName";
    public static final String LASTNAME_PASS_MESSAGE = "Password contains lastName";
    public static final String EASY_PASS_MESSAGE = "Password is easy";
    public static final String NEGATIVE_BALANCE_MESSAGE = "Money mist be positive value";
    public static final String INVALID_LOGIN_MESSAGE = "Invalid login";
    public static final String SYMBOLS_FIRSTNAME_MESSAGE = "First name should begin with upper case letter" +
            " and shouldn't contain any symbols or digits or other uppercase letters";
    public static final String SHORT_FIRSTNAME_MESSAGE = "First name should be at least 2 symbols";
    public static final String LONG_FIRSTNAME_MESSAGE = "First name shouldn't be longer than 12 symbols";
    public static final String SYMBOLS_LASTNAME_MESSAGE = "Last name should begin with upper case letter" +
            " and shouldn't contain any symbols or digits or other uppercase letters";
    public static final String SHORT_LASTNAME_MESSAGE = "Last name should be at least 2 symbols";
    public static final String LONG_LASTNAME_MESSAGE = "Last name shouldn't be longer than 12 symbols";

    public CustomerException(String msg) {
        super(msg);
    }
}