package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlanException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class ServicePlan {
    private UUID id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;

    public ServicePlan(String name, String details, int maxSeats, int minSeats, int feePerUnit) throws ServicePlanException {
        this.id = UUID.randomUUID();
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;

        validate(name, details, maxSeats, minSeats, feePerUnit);
    }

    private void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit) throws ServicePlanException {
        validateName(name);
        validateDetails(details);
        validateMaxSeats(maxSeats);
        validateMinSeats(minSeats, maxSeats);
        validateFee(feePerUnit);
    }

    private void validateName(String name) throws ServicePlanException {
        if (name.length() > 128)
            throw new ServicePlanException(ServicePlanException.LONG_NAME_MESSAGE);
        if (name.length() < 2)
            throw new ServicePlanException(ServicePlanException.SHORT_NAME_MESSAGE);

        if (name.matches(".*[@#$%&].*"))
            throw new ServicePlanException(ServicePlanException.SYMBOLS_NAME_MESSAGE);
    }

    private void validateDetails(String details) throws ServicePlanException {
        if (details.length() > 1024)
            throw new ServicePlanException(ServicePlanException.LONG_DETAILS_MESSAGE);
        if (details.length() < 1)
            throw new ServicePlanException(ServicePlanException.SHORT_DETAILS_MESSAGE);
    }

    private void validateMaxSeats(int maxSeats) throws ServicePlanException {
        if (maxSeats > 999999)
            throw new ServicePlanException(ServicePlanException.BIG_MAXSEATS_MESSAGE);
        if (maxSeats < 1)
            throw new ServicePlanException(ServicePlanException.SMALL_MAXSEATS_MESSAGE);
    }

    private void validateMinSeats(int minSeats, int maxSeats) throws ServicePlanException {
        if (minSeats > 999999)
            throw new ServicePlanException(ServicePlanException.BIG_MINSEATS_MESSAGE);
        if (minSeats < 1)
            throw new ServicePlanException(ServicePlanException.SMALL_MINSEATS_MESSAGE);

        if (minSeats > maxSeats)
            throw new ServicePlanException(ServicePlanException.MINSEATS_MORE_MAXSEATS_MESSAGE);
    }

    private void validateFee(int fee) throws ServicePlanException {
        if (fee < 0)
            throw new ServicePlanException(ServicePlanException.NEGATIVE_FEE_MESSAGE);
        if (fee > 999999)
            throw new ServicePlanException(ServicePlanException.BIG_FEE_MESSAGE);
    }
}
