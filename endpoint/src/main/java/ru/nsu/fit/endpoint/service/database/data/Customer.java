package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.validator.routines.EmailValidator;
import ru.nsu.fit.endpoint.service.database.exceptions.CustomerException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer {
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    /* счет не может быть отрицательным */
    private int money;

    public Customer(String firstName, String lastName, String login, String pass, int money) throws CustomerException {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.money = money;
        validate(firstName, lastName, login, pass, money);
    }

    public void validate(String firstName, String lastName, String login, String pass, int money) throws CustomerException {
        // validate password
        validatePassword(pass);
        // validate login
        validateLogin(login);
        // validate names
        validateNames(firstName, lastName);
        // validate money
        if (money < 0)
            throw new CustomerException(CustomerException.NEGATIVE_BALANCE_MESSAGE);
    }

    private void validateNames(String firstName, String lastName) throws CustomerException {
        // firstname
        if (!firstName.matches("^[A-Z][a-z]*"))
            throw new CustomerException(CustomerException.SYMBOLS_FIRSTNAME_MESSAGE);

        if (firstName.length() < 2)
            throw new CustomerException(CustomerException.SHORT_FIRSTNAME_MESSAGE);
        if (firstName.length() > 12)
            throw new CustomerException(CustomerException.LONG_FIRSTNAME_MESSAGE);

        // lastname
        if (!lastName.matches("^[A-Z][a-z]*"))
            throw new CustomerException(CustomerException.SYMBOLS_LASTNAME_MESSAGE);

        if (lastName.length() < 2)
            throw new CustomerException(CustomerException.SHORT_LASTNAME_MESSAGE);
        if (lastName.length() > 12)
            throw new CustomerException(CustomerException.LONG_LASTNAME_MESSAGE);
    }

    private void validateLogin(String login) throws CustomerException {
        EmailValidator validator = EmailValidator.getInstance();

        if (!validator.isValid(login))
            throw new CustomerException(CustomerException.INVALID_LOGIN_MESSAGE);
    }

    private void validatePassword(String pass) throws CustomerException {
        if (pass == null || pass.equals(""))
            throw new CustomerException(CustomerException.EMPTY_PASS_MESSAGE);

        if (pass.length() < 6)
            throw new CustomerException(CustomerException.SHORT_PASS_MESSAGE);
        if (pass.length() >= 13)
            throw new CustomerException(CustomerException.LONG_PASS_MESSAGE);

        if (!isStrongPassword(pass))
            throw new CustomerException(CustomerException.EASY_PASS_MESSAGE);

        if (pass.contains(login))
            throw new CustomerException(CustomerException.LOGIN_PASS_MESSAGE);
        if (pass.contains(firstName))
            throw new CustomerException(CustomerException.FIRSTNAME_PASS_MESSAGE);
        if (pass.contains(lastName))
            throw new CustomerException(CustomerException.LASTNAME_PASS_MESSAGE);
    }

    private boolean isStrongPassword(String pass) {
        if ((!pass.matches(".*[a-z].*")) ||
            (!pass.matches(".*[A-Z].*")) ||
            (!pass.matches(".*\\d.*")))
                return false;

        return true;
    }
}
