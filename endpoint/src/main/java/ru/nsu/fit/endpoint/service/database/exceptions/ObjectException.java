package ru.nsu.fit.endpoint.service.database.exceptions;

public class ObjectException extends Exception {
    public ObjectException() {
        super();
    }
    public ObjectException(String msg) {
        super(msg);
    }
}